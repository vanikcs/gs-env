# Автоматическая настройка стендов
### Настройка ssh
Настроить hosts лишним не будет:
```
cat << EOF >> /etc/hosts
192.168.55.6      pve05.local pve05
192.168.55.7      pve06.local pve06
192.168.55.8      pve07.local pve07
EOF
```
Сгенирировать и скопировать ключи:
```
ssh-keygen
ssh-copy-id root@pve05
```

### Настройка ansible
```
apt install -y ansible
ansible-galaxy collection install community.general
```

### Настройка автодополнения ansible
Генерация скриптов автодополнения:
```
register-python-argcomplete3 ansible > /etc/bash_completion.d/ansible.sh
register-python-argcomplete3 ansible-config > /etc/bash_completion.d/ansible-config.sh
register-python-argcomplete3 ansible-console > /etc/bash_completion.d/ansible-console.sh
register-python-argcomplete3 ansible-doc > /etc/bash_completion.d/ansible-doc.sh
register-python-argcomplete3 ansible-galaxy > /etc/bash_completion.d/ansible-galaxy.sh
register-python-argcomplete3 ansible-inventory > /etc/bash_completion.d/ansible-inventory.sh
register-python-argcomplete3 ansible-playbook > /etc/bash_completion.d/ansible-playbook.sh
register-python-argcomplete3 ansible-pull > /etc/bash_completion.d/ansible-pull.sh
register-python-argcomplete3 ansible-vault > /etc/bash_completion.d/ansible-vault.sh
```

### Запуск скриптов
Создание виртуальной машины:
```
sh _deploy_cluster.sh <username исполнителя playbook-а>
```
